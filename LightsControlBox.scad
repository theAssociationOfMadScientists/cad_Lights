// begin arduino base plate
//board specs
board_width		=	53;
board_depth		=	68.5;
thickness		=	.5;
board_thickness	=	2;
component_height=	10;
// peg params
peg_diameter	=	2;
peg_radius		=	peg_diameter/2;
peg_ball		=	peg_diameter+1;
peg_height		=	board_thickness+thickness*2;
peg_slit		=	[peg_ball,peg_ball,peg_height];
peg_1_pos		=	[ peg_diameter, 15, 0 ];
peg_2_pos		=	[ 15-peg_diameter, board_depth-peg_diameter, 0 ];
peg_3_pos		=	[ board_width-10-peg_diameter, board_depth-peg_diameter,0 ];
peg_4_pos		=	[ board_width-peg_diameter, 15, 0 ];
// lid params
lid_cutout		=	[board_width,board_depth,component_height];
lid				=	lid_cutout+[thickness,thickness,thickness];
pinHeader1_depth=	40;
pinHeader2_depth=	50;
pinHeader_width	=	3;
USB_port_width	=	12;
USB_port_depth	=	10;
USB_port_offsetX=	9;
PWR_port_width	=	9;
PWR_port_depth	=	11;
PWR_port_offsetX=	41;

module arduinoBase(){
	cube ([ board_width,board_depth,thickness ]);
}
module peg(){
	difference(){
		union(){
			translate ([0,0,peg_height ])
				scale ([peg_diameter/peg_ball,1,1])
				sphere(d=peg_ball, $fs=1);	// the ball atop the pegs
			cylinder(peg_height,d=peg_diameter);	// the peg...support? the board rests wrapped around this part
		}
		translate ([ 0,0,peg_height-board_thickness+0.01])	
			scale( [peg_ball/(peg_ball-peg_diameter),1,1])
			rotate([0,0,45])
			cylinder( $fn=4, d1=0, d2=board_thickness, h=peg_height+thickness );	// the slit in the middle to allow the peg to squeeze
	}
}
// End code taken from ~/Documents/Designs/ArduinoMount/ArduinoMount.scad

module lid(){
	difference(){
		cube(size=lid);
		translate([thickness/2,thickness/2,-.01]) cube(size=lid_cutout);
		pin_header_holes();
		port_holes();
	}
	latch(side=1);
	latch(side=2);
	latch(side=3);
	latch(side=4);
}
module latch(side){
	if (side==1){
		rotate ([ 90,0,90 ])
		translate ([ 0,0,board_width/2-5 ])
			cylinder(r=thickness,h=10,$fs=.25);
	}
	else if (side==2){
		rotate ([ 90,0,180 ])
		translate ([ 0,0,board_depth/2-5 ])
			cylinder(r=thickness,h=10,$fs=.25);
	}
	else if (side==3){
		rotate ([ 90,0,90 ])
		translate ([ board_depth+thickness*3/4,0,board_width/2-5 ])	// strange value probably doesn't scale... :(
			cylinder(r=thickness,h=10,$fs=.25);
	}
	else if (side==4){
		rotate ([ 90,0,180 ])
		translate ([ -(board_width+thickness*.75),0,board_depth/2-5 ])
			cylinder(r=thickness,h=10,$fs=.25);
	}else{
		echo("Error, latch side is ", side);
	}
}

module pin_header_holes(){
	translate([ board_width-thickness-pinHeader_width, board_depth-2-pinHeader1_depth, component_height-.1])
		cube([ pinHeader_width,pinHeader1_depth,thickness+.2 ]);
	translate([ pinHeader_width+thickness, board_depth-2-pinHeader2_depth, component_height-.1 ])
		cube([pinHeader_width,pinHeader2_depth,thickness+.2]);
}
module port_holes(){
	translate ([ USB_port_offsetX,-.01,-.01 ])
		cube ([ USB_port_width, USB_port_depth+.1,component_height+board_thickness+.2 ]);
	translate([PWR_port_offsetX, -.1,-.01 ])
		cube( [PWR_port_width, PWR_port_depth, component_height+board_thickness+.2 ]);
}
union(){
	// arduino baseplate modules
	%difference(){
		union(){
			arduinoBase();
			translate ( peg_1_pos ) peg();
			translate ( peg_2_pos ) peg();
			translate ( peg_3_pos ) peg();
			translate ( peg_4_pos ) peg();
		}
		translate([ 7, 7, -.01]) cube([ 25, board_depth-14, thickness+.02]);	//cutout in the bottom
	}
	
	// end baseplate modules
	translate( [-thickness,-thickness,10] ) lid();
}