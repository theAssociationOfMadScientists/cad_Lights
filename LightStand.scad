leg_outside_diameter = .7*25.4;	// this value is supposedly the approximate OD of 1/2 inch EMT conduit, change this if you find a more appropriate material.
main_chunk_rad	= 75;
leg_length		= main_chunk_rad*1.25; // arbitrary value larger than the overall shape
pin_diameter	= 2.5;
pin_length		= 2;
pin_hook		= 3;
coin_radius		= leg_outside_diameter/2+pin_length;

// modules
module leg(){
    rotate([0,180-60,0]) cylinder(d=leg_outside_diameter, h=leg_length);
}
module legSpace(){
    translate([0,-leg_outside_diameter/2,0])
		rotate([-90,0,0])
		linear_extrude(leg_outside_diameter)
		polygon([
      	[leg_length*1.75,leg_length],
			[0,leg_length],
			[0,0]
		]);
}
module verticalSupport(){
    translate([0,0,leg_outside_diameter])
		cylinder(d=leg_outside_diameter, h=leg_length);
}
module pinSlot(){
	rotate([0,120,0]){
		translate([0,-(leg_outside_diameter+pin_length*2)/2,0])
			//rotate([0,0,0])
			linear_extrude(20)
			square([pin_diameter,leg_outside_diameter+pin_length*2]);
		translate([pin_hook+pin_diameter/2,
			-(leg_outside_diameter+pin_length*2)/2,
			20+pin_hook/2])
			rotate([270,135,0])
			rotate_extrude(angle=275,convexity=5)
			translate([pin_hook,0,0])
			square([pin_diameter,leg_outside_diameter+pin_length*2]);
		translate([pin_diameter*2+.01,
			-(leg_outside_diameter+pin_length*2)/2-.02,
			20-pin_diameter-pin_hook])
			cube([ pin_diameter
				,leg_outside_diameter+pin_length*2+.05,
				pin_diameter+pin_hook ]);
	}
}
module coin(){
	cylinder(r=coin_radius, h=3);
}

module coinSlot(){
	difference(){
		translate([ (20-pin_diameter-pin_hook)*sin(60)-pin_hook-pin_diameter, 0, -(20)*cos(60)-pin_hook])
			rotate([90,110,0])
			rotate_extrude(angle=90,convexity=10)
			translate([(main_chunk_rad-(main_chunk_rad/8))-(20)*cos(60)-pin_hook+1.5,-coin_radius,0])
			square([3,coin_radius*2]);
	}
	rotate([0,30,0])
		translate([(main_chunk_rad-(main_chunk_rad/8)),0,0])
		rotate([0,90,0])
		coin();
}
module coinSpace(){
	cylinder(r=coin_radius, h=main_chunk_rad/8+5);
}
// excess bits
module XS(){
	translate([0,0,main_chunk_rad+main_chunk_rad/10])
		rotate_extrude(angle=360, convexity=3)
		translate( [main_chunk_rad+leg_outside_diameter*2, 0]) 
		circle(main_chunk_rad+leg_outside_diameter);
	translate([0,0,-main_chunk_rad])
		cylinder(r=leg_outside_diameter*2, h=8);
}
// excess that should be rotated triangularly
module rotXS(){
	translate([ leg_outside_diameter*.5,leg_outside_diameter*.75,])
	rotate([180,0,120])
    rotate_extrude(angle=120, convexity=10)
		polygon([
		[10,10],
		[10,main_chunk_rad*2],
		[main_chunk_rad*2,10]]);
}

// assembly of modules
difference(){
	//main chunk
	sphere(r=main_chunk_rad);
	for (i=[0,120,240]){
		rotate([0,0,i]) leg();
		rotate([0,0,i]) legSpace();
		rotate([0,0,i]) pinSlot();
		rotate([0,0,i]) coinSlot();
		rotate([0,0,i]) rotXS();
	}
	verticalSupport();
	translate([0,0,-main_chunk_rad-5])
		cylinder(d=leg_outside_diameter, h=leg_length);
	translate([0,0,-main_chunk_rad])
		coinSpace();
	XS();
}